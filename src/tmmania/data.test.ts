import {
    getHighestStatValue,
    getTopFourAverageStatValues,
    getTopThreeAverageStatValues,
    getTopTwoAverageStatValues,
    getTotal,
} from "./data"

test("stat total returns expected", () => {
    const result = getTotal(2.5)

    expect(result.value).toEqual(63)
})

test("highest stat returns expected", () => {
    const result = getHighestStatValue(2.5)

    expect(result.value).toEqual(23)
})

test("two stat returns expected", () => {
    const result = getTopTwoAverageStatValues(2.5)

    expect(result.value).toEqual(20)
})

test("three stat returns expected", () => {
    const result = getTopThreeAverageStatValues(2.5)

    expect(result.value).toEqual(16.66)
})

test("four stat returns expected", () => {
    const result = getTopFourAverageStatValues(2.5)

    expect(result.value).toEqual(14.25)
})
